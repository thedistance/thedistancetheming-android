package uk.co.thedistance.thedistancetheming.fonts;

import android.databinding.BindingAdapter;
import android.view.View;

/**
 * Custom {@link BindingAdapter} methods relating to fonts
 */
public class Bindings {

    /**
     * Apply a font to a view
     * @param view The view to apply a font to
     * @param fontName The name of the font file, sans extension
     */
    @BindingAdapter({"font"})
    public static void setFont(View view, String fontName) {
        Font.setFont(view, fontName);
    }
}
