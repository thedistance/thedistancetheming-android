package uk.co.thedistance.thedistancetheming.fonts;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

/**
 * Simple class used for applying fonts to views
 */
public class Font {

    private static final String TAG = "Font";

    /**
     * Apply a font to a view
     *
     * @param view     The view to apply a font to, must be an instance of {@link TextView}, {@link CollapsingToolbarLayout} or {@link TextInputLayout}.
     *                 To add support for more classes, override this method.
     * @param fontName The name of the font file, sans extension
     * @return true if the font was applied to the view, false otherwise
     */
    public static boolean setFont(View view, String fontName) {
        Context context = view.getContext().getApplicationContext();
        FontCache cache = FontCache.getInstance(context);
        Typeface typeface = cache.get(fontName);

        if (typeface == null) {
            Log.i(TAG, "setFont: failed to set font; font '" + fontName + "'");
            return false;
        }

        if (view instanceof TextView) {
            ((TextView) view).setTypeface(typeface);
            ((TextView) view).setPaintFlags(((TextView) view).getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG);
            return true;
        } else if (view instanceof CollapsingToolbarLayout) {
            ((CollapsingToolbarLayout) view).setExpandedTitleTypeface(typeface);
            ((CollapsingToolbarLayout) view).setCollapsedTitleTypeface(typeface);
        } else if (view instanceof TextInputLayout) {
            ((TextInputLayout) view).setTypeface(typeface);
            return true;
        } else if (view instanceof Toolbar) {
            FontSpan span = new FontSpan(typeface);
            SpannableString title = new SpannableString(((Toolbar) view).getTitle());
            title.setSpan(span, 0, title.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            ((Toolbar) view).setTitle(title);
            return true;
        } else if (view instanceof TextInputLayout) {
            ((TextInputLayout) view).setTypeface(typeface);
            return true;
        }

        Log.i(TAG, "setFont: failed to set font; class '" + view.getClass().getSimpleName() + "' not supported");

        return false;
    }
}
