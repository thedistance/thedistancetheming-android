package uk.co.thedistance.thedistancetheming.fonts;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.support.v4.util.ArrayMap;
import android.util.Log;

import java.io.IOException;

/**
 * A simple font cache that makes a font once when it's first asked for and keeps it for the
 * life of the application.
 * <p>
 * To use it, put your fonts in /assets/fonts.  You can access them in XML by their filename, minus
 * the extension (e.g. "Roboto-BoldItalic" or "roboto-bolditalic" for Roboto-BoldItalic.ttf).
 * <p>
 * To set custom names for fonts other than their filenames, call addFont().
 */
public class FontCache {

    private static final String DIR_FONTS = "fonts";
    private static final String TAG = "FontCache";
    private static FontCache sInstance;
    private final Context context;
    private ArrayMap<String, Typeface> cache = new ArrayMap<>();
    private ArrayMap<String, String> fontMapping = new ArrayMap<>();

    /**
     * Get a singleton instance, pre-populated from fonts in the /assets/fonts directory
     *
     * @param context The current Application context
     * @return {@link FontCache} instance
     */
    public static FontCache getInstance(Context context) {
        synchronized (FontCache.class) {
            if (sInstance == null) {
                sInstance = new FontCache(context);
            }
            return sInstance;
        }
    }

    private FontCache(Context context) {
        this.context = context;
        AssetManager am = context.getResources().getAssets();
        String fileList[];
        try {
            fileList = am.list(DIR_FONTS);
        } catch (IOException e) {
            Log.e(TAG, "Error loading fonts from assets/fonts.");
            return;
        }

        for (String filename : fileList) {
            String alias = filename.substring(0, filename.lastIndexOf('.'));
            fontMapping.put(alias, filename);
            fontMapping.put(alias.toLowerCase(), filename);
        }
    }

    /**
     * Add a font to the font mapping. Useful for fonts not placed in the 'fonts' directory
     *
     * @param name         The name used to reference the font in XML
     * @param fontFilename The filename / path of the font file
     */
    public void addFont(String name, String fontFilename) {
        fontMapping.put(name, fontFilename);
    }

    /**
     * Retrieve the relevant {@link Typeface} from the cache. If not already cached, the font will
     * be loaded at this point
     *
     * @param fontName The font to load
     * @return The {@link Typeface} for the given name. null if not found
     */
    public Typeface get(String fontName) {
        String fontFilename = fontMapping.get(fontName);
        if (fontFilename == null) {
            Log.e(TAG, "Couldn't find font " + fontName + ".");
            return null;
        }
        if (cache.containsKey(fontName)) {
            return cache.get(fontName);
        } else {
            Typeface typeface = Typeface.createFromAsset(context.getAssets(), DIR_FONTS + "/" + fontFilename);
            cache.put(fontFilename, typeface);
            return typeface;
        }
    }
}
