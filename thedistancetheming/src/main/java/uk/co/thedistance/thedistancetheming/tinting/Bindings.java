package uk.co.thedistance.thedistancetheming.tinting;

import android.content.res.ColorStateList;
import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * Custom {@link BindingAdapter} methods for adding tints to views and drawables
 */
public class Bindings {

    /**
     * Apply a color filter to all compound Drawables in a TextView
     */
    @BindingAdapter("bind:compoundTint")
    public static void tintDrawable(TextView textView, @ColorInt int color) {
        for (Drawable drawable : textView.getCompoundDrawables()) {
            if (drawable != null) {
                Drawable wrapped = DrawableCompat.wrap(drawable);
                wrapped.mutate();
                DrawableCompat.setTint(wrapped, color);
            }
        }
    }

    /**
     * Apply a tint to the background of the given {@link View}
     */
    @BindingAdapter("bind:viewTint")
    public static void tintView(View view, @ColorInt int color) {
        if (view.getBackground() != null) {

            if (view instanceof AppCompatButton) {
                AppCompatButton button = (AppCompatButton) view;
                ColorStateList csl = new ColorStateList(new int[][]{new int[0]}, new int[]{color});
                button.setSupportBackgroundTintList(csl);
                return;
            }
            Drawable drawable = DrawableCompat.wrap(view.getBackground());
            drawable.mutate();
            DrawableCompat.setTint(drawable, color);
        }
    }

    /**
     * Apply a color filter to an {@link ImageView}
     */
    @BindingAdapter("bind:drawableTint")
    public static void tintDrawable(ImageView imageView, @ColorInt int color) {
        if (imageView.getDrawable() == null) {
            return;
        }
        Drawable drawable = DrawableCompat.wrap(imageView.getDrawable());
        drawable.mutate();
        DrawableCompat.setTint(drawable, color);
        imageView.setImageDrawable(drawable);
    }

    /**
     * Apply a tint to the {@link Toolbar} navigation icon
     */
    @BindingAdapter("bind:navigationTint")
    public static void tintNavigation(Toolbar toolbar, @ColorInt int color) {
        Drawable wrapped = DrawableCompat.wrap(toolbar.getNavigationIcon());
        wrapped.mutate();
        DrawableCompat.setTint(wrapped, color);
        toolbar.setNavigationIcon(wrapped);
    }
}
