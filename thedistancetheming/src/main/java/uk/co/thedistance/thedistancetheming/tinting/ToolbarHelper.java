package uk.co.thedistance.thedistancetheming.tinting;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.MenuRes;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

/**
 * Created by benbaggley on 02/06/15.
 */
public class ToolbarHelper {

    Toolbar toolbar;
    Resources resources;

    public static ToolbarHelper with(@NonNull Toolbar toolbar) {
        return new ToolbarHelper(toolbar);
    }

    private ToolbarHelper(@NonNull Toolbar toolbar) {
        this.toolbar = toolbar;
        this.resources = toolbar.getResources();
    }

    /**
     * Simple method for setting a navigation icon and applying a tint
     * @return the current {@link ToolbarHelper} instance
     */
    public ToolbarHelper setNavigationIcon(@DrawableRes int resId, @ColorInt int color) {
        Drawable drawable = DrawableCompat.wrap(resources.getDrawable(resId));
        DrawableCompat.setTint(drawable, color);
        toolbar.setNavigationIcon(drawable);

        return this;
    }

    /**
     * Simple method for inflating a menu and tinting any icons
     * @return the current {@link ToolbarHelper} instance
     */
    public ToolbarHelper setMenu(@MenuRes int menuId, @ColorInt int color) {
        toolbar.inflateMenu(menuId);

        Menu menu = toolbar.getMenu();
        if (menu != null) {
            for (int i = 0; i < menu.size(); i++) {
                MenuItem item = menu.getItem(i);
                if (item.getIcon() != null) {
                    Drawable icon = DrawableCompat.wrap(item.getIcon());
                    DrawableCompat.setTint(icon, color);
                }
            }
        }

        return this;
    }
}
